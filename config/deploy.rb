set :application, 'nimbus'
set :repo_url, 'git@bitbucket.org:andrezimpel/nimbus.git'

set :deploy_to, '/www/dev/nimbus'

# user
set :use_sudo, true

set :format, :pretty
set :log_level, :debug
set :pty, true


namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
    end
  end
  after :finishing, 'deploy:cleanup'

  before :restart, :build_public do
    on roles(:app) do
      within release_path do
        # execute "jekyll build --destination public"
        execute "cd #{deploy_to}/current && bundle exec jekyll build --destination public"
      end
    end
  end

  after :publishing, :restart
end
